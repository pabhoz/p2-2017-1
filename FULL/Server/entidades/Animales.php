<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Animales
 *
 * @author pabhoz
 */

class Animales {

    private $id;
    private $nombre;
    private $hace;

    protected static $table = "animales";

    function __construct($id,string $nombre,string $hace) {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->hace = $hace;
    }


    function getId() {
        return $this->id;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getHace() {
        return $this->hace;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setHace($hace) {
        $this->hace = $hace;
    }

    public function getAll(){
      $db = new DataBase(DB_HOST, DB_USER, DB_PASS, DB_NAME);
      return $db->select("*", self::$table);
    }

}
