-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema parcial
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `parcial` ;

-- -----------------------------------------------------
-- Schema parcial
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `parcial` DEFAULT CHARACTER SET utf8 ;
USE `parcial` ;

-- -----------------------------------------------------
-- Table `parcial`.`Animales`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `parcial`.`Animales` ;

CREATE TABLE IF NOT EXISTS `parcial`.`Animales` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `hace` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `parcial`.`Animales`
-- -----------------------------------------------------
START TRANSACTION;
USE `parcial`;
INSERT INTO `parcial`.`Animales` (`id`, `nombre`, `hace`) VALUES (DEFAULT, 'Vaca', 'Muu');
INSERT INTO `parcial`.`Animales` (`id`, `nombre`, `hace`) VALUES (DEFAULT, 'Perro', 'Woof');
INSERT INTO `parcial`.`Animales` (`id`, `nombre`, `hace`) VALUES (DEFAULT, 'Gato', 'Miau');

COMMIT;

