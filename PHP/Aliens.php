<?php
/*
  Defina la clase alien que escriba la base de todos los
  aliens.

  Todos los aliens de nuestra historia se comunican telepaticamente
  así que defina una constante COMUNICACION con el valor
  "telepáticamente".

  Los aliens tienen un $nombre, $edad, $especie, $planeta
  y sus atributos deben estar encapsulados.

  Los alines de Marte siempre tienen como $planeta el valor
  "marte" y los de Jupiter el valor "jupiter".

  Los aliens usan el método interactuar que por defecto retorna
  "[COMUNICACION] dice: Hola terricola mi nombre es [NOMBRE], vinimos en son de paz"

  Los aliens de Marte son agresivos así que al interactuar
  retorna "[COMUNICACION] dice: Hola terricola, rindanse ante la invasión de [NOMBRE]"

  Los aliens de Jupiter son los unicos que hablan con acento
  (telepaticamente) así que todas las "s" las pronuncian como
  "sh"

  Debe crear los métodos Setters y Getters

  Aplique los conceptos de herencia para desarrollar las clases
  de cada uno de los aliens descritos, use la siguiente base:
 */
class Alien{

}

class MarsAlien{

}

class JupiterAlien{

}

$malien = new MarsAlien("Ron","345","Marticopulus");
$jalien = new JupiterAlien("Wen","215","Jupiroculus");
$et = new Alien("E.T.","28","Montacicletus","Plutón");

print $malien->interactuar();
print "</br>";
print $jalien->interactuar();
print "</br>";
print $et->interactuar();
