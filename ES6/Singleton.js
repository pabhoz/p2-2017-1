/*
  Cree un método get en el objeto ids que retorne el id
  actual y lo incremente para su proxima llamada
*/
var ids = {
  next: 0
}

console.log(ids.get())
// → 0
console.log(ids.get())
// → 1
