/*
Escriba una clase llamada Point que represente un punto
en un espacio 3D. Recuerde que un punto tienes coordenadas
x,y,z que deberán ser pasadas como argumentos al constructor.

La clase tendrá un método llamado sumar, que recibe otro punto y retorna un
nuevo punto resultante.

Nota: recuerde definir los getters y setters de la clase
*/
// Your code here

console.log(new Point(1, 2, 4).sumar(new Point(2, 1, 1)))
// → Point{_x: 3, _y: 3, _z: 5}
