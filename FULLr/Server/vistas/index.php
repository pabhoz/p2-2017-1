<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
</head>

<body>
    <ul>
        Los Animalitos de mi granja:
        <?php foreach ($animales as $animal): ?>
          <li>Soy un <?php print $animal["nombre"]; ?> y hago <?php print $animal["hace"]; ?></li>
        <?php endforeach; ?>
    </ul>

    <script>
          setInterval(function(){
            console.log("Llamando a la DB");
          }, 5000);

    </script>
</body>

</html>
